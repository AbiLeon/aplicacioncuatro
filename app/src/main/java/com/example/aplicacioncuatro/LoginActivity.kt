package com.example.aplicacioncuatro

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : AppCompatActivity(), View.OnClickListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        btn_login_init.setOnClickListener(this)


        if(DataPreferences.getStoredRFC(context = this).isNotEmpty()){
            MainActivity.launchActivity(context = this, rfc = DataPreferences.getStoredRFC(context = this) )
            finish()
        }
    }

    override fun onClick(p0: View?) {
        when(p0!!.id){
            btn_login_init.id ->{
                MainActivity.launchActivity(context = this, rfc = tie_login_rfc.text.toString() )
                finish()
            }
        }
    }
}
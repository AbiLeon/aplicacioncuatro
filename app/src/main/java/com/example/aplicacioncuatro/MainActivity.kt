package com.example.aplicacioncuatro

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*
import java.util.*

class MainActivity : AppCompatActivity() {

    companion object {
        const val KEY_PARAM_RFC = "com.example.aplicacioncuatro.KEY_PARAM_RFC"

        fun launchActivity(context: Context, rfc: String) {
            val intent = Intent(context, MainActivity::class.java)
            intent.putExtra(KEY_PARAM_RFC, rfc)
            context.startActivity(intent)
        }
    }

    private var rfc = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setUpToolbar(title = MainActivity::class.java.simpleName, enabledMenuOpt = false)
        setContentView(R.layout.activity_main)
        checkExtras()
        showData()
    }

    private fun setUpToolbar(title: String, enabledMenuOpt: Boolean) {
        val actionBar = supportActionBar
        if (actionBar != null) {
            actionBar.show()
            actionBar.title = title
            actionBar.setDisplayShowTitleEnabled(true)
            actionBar.setDisplayHomeAsUpEnabled(enabledMenuOpt)
            actionBar.setDisplayShowHomeEnabled(enabledMenuOpt)
        }
    }

    private fun showData() {
        tv_main_rfc.text = rfc.toUpperCase(Locale("es", "MX"))
    }

    private fun checkExtras() {
        intent.extras!!.let {
            if (it.containsKey(KEY_PARAM_RFC)) {
                rfc = it.getString(KEY_PARAM_RFC)!!
                DataPreferences.setStoredRFC(this, rfc)
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater: MenuInflater = menuInflater
        inflater.inflate(R.menu.options_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.action_other -> {
                Toast.makeText(this, "Se seleciono otra opción", Toast.LENGTH_LONG).show()
                true
            }
            R.id.action_help -> {
                HelpActivity.launchActivity(context = this)
                true
            }
            else -> onOptionsItemSelected(item)
        }
    }


}
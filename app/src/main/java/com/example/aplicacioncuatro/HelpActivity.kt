package com.example.aplicacioncuatro

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle

class HelpActivity : AppCompatActivity() {

    companion object{

        fun launchActivity(context: Context){
            val intent = Intent(context, HelpActivity::class.java)
            context.startActivity(intent)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_help)
        setUpToolbar(title = HelpActivity::class.java.simpleName, enabledMenuOpt = true)
    }


    private fun setUpToolbar(title: String, enabledMenuOpt: Boolean) {
        val actionBar = supportActionBar
        if (actionBar != null) {
            actionBar.show()
            actionBar.title = title
            actionBar.setDisplayShowTitleEnabled(true)
            actionBar.setDisplayHomeAsUpEnabled(enabledMenuOpt)
            actionBar.setDisplayShowHomeEnabled(enabledMenuOpt)
        }
    }

    override fun onSupportNavigateUp() : Boolean {
        onBackPressed()
        return true
    }

    override fun onBackPressed() {
        super.onBackPressed()
    }
}